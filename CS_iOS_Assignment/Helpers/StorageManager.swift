//
//  PathManager.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

private enum StorageKeys: String {
    case cacheStoredTime = "cacheStoredTime"
}

class StorageManager {
    static func directory() -> URL? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("cachedFiles")
    }

    static func namedDirectory(for url: String, ext: String) -> URL? {
        guard let cachedFiles = self.directory() else { return nil }
        
        var isDirectory = ObjCBool(false)
        let exists = FileManager.default.fileExists(atPath: cachedFiles.path, isDirectory: &isDirectory)
        if  exists && isDirectory.boolValue == true  {
            return cachedFiles.appendingPathComponent(url + "." + ext)
        }
        do {
            try FileManager.default.createDirectory(at: cachedFiles, withIntermediateDirectories: true, attributes: nil)
        }
        catch let error {
            print(error)
        }
        
        return cachedFiles.appendingPathComponent(url + "." + ext)
    }
  
    static func removeAt(_ url: URL) throws{
        try FileManager.default.removeItem(at: url)
    }
  
    static func copyItem(from source: URL, to destination: URL) throws {
        try FileManager.default.copyItem(at: source, to: destination)
    }
    
    static func dataPresented(url: URL) -> Bool {
        FileManager.default.fileExists(atPath: url.absoluteString)
    }
    
    static func destinationURL(from url: URL) -> URL? {
        var deviation = ""
        if let query = url.query {
            deviation = query.split(separator: "&")
                .map{ String($0.split(separator: "=").last ?? "") }
                .reduce("", { $0 + $1 })
        }
        let path = url.lastPathComponent + deviation
        return self.namedDirectory(for: path, ext: "data")
    }
}

//MARK: Cache
extension StorageManager {
    static func clearCache() {
        guard let cachedFiles = self.directory() else { return }
        do {
            try DispatchQueue.init(label: "movies.cacheQueue").sync {
                try FileManager.default.removeItem(at: cachedFiles)
                try FileManager.default.createDirectory(at: cachedFiles, withIntermediateDirectories: true, attributes: nil)
            }
        } catch {
            return
        }
    }
 
    static func setCacheTime(date: Date) {
        UserDefaults.standard.set(date, forKey: StorageKeys.cacheStoredTime.rawValue)
    }
    
    static func getCacheTime() -> Date? {
        UserDefaults.standard.value(forKey: StorageKeys.cacheStoredTime.rawValue) as? Date
    }
    
}
