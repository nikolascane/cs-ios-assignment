//
//  CacheManager.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 01.05.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct CacheManager {
    static func refreshAppCacheIfNeeded() -> Bool {
        guard let lastCachedTime = StorageManager.getCacheTime() else {
            self.setCacheTime()
            return true
        }
        guard Date().distance(to: lastCachedTime) <= 0 else {
            return false
        }
        StorageManager.clearCache()
        self.setCacheTime()
        return true
    }
    
    static func setCacheTime() {
        let nextClearTime: TimeInterval = ServiceManager.shared.formatService.cacheLimitTime
        let nextCacheClearDate = Date(timeIntervalSinceNow: nextClearTime)
        StorageManager.setCacheTime(date: nextCacheClearDate)
    }
}
