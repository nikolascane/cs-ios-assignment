//
//  RouterService.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

struct Route {
    let path: Path
    var screenName: String? = nil
    var storyboardName: String? = nil
    
    enum Path {
        case movieList
        case movieDetails
        case back
        case none
    }
}

enum ConfigPath {
    case movieList
    case nowPlaying
    case movieDetails
    case none
}

enum NavigationError: Swift.Error, CustomStringConvertible {
    case cannotPresent(PresentationMode, String)
    case sourceError
    
    var description: String {
        switch self {
        case .cannotPresent(let mode, let description):
            return "Cannot present view controller \(mode.rawValue) mode. Reason: \(description)"
        case .sourceError:
            return "Error in finding controller to present from"
        }
    }
}

enum PresentationMode: String {
    case push
    case modal
    case modalTransparent
    case none
}

struct RouteAction {
    let config: AnyObject?
    let route: Route?
    let configPath: ConfigPath?
    let presentationMode: PresentationMode
}

protocol RouterProtocol: class {
    var routes: [Route.Path] {get}
    var configs: [ConfigPath] {get}
    var currentStoryboardName: String {get}
    
    func typeOf(path: ConfigPath) -> UIViewController.Type 
    func canRoute(action: RouteAction) -> Bool
    func canConfig(action: RouteAction) -> Bool
    func config(viewController: UIViewController, action: RouteAction) -> Bool
    func createViewController(action: RouteAction) -> UIViewController?
    func instantiateViewController(route: Route?) -> UIViewController?
}

extension RouterProtocol {
    func canRoute(action: RouteAction) -> Bool {
        if let path = action.route?.path {
            return self.routes.contains(path)
        }
        return false
    }
    
    func canConfig(action: RouteAction) -> Bool {
        if let configPath = action.configPath {
            return self.configs.contains(configPath)
        }
        return false
    }
    
    func instantiateViewController(route: Route?) -> UIViewController? {
        guard let storyboardName = route?.storyboardName, let screenName = route?.screenName else { return nil }
        return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(identifier: screenName)
    }
    
    func initialViewContoller(route: Route) -> UIViewController? {
        if let storyboardName = route.storyboardName {
            return UIStoryboard(name: storyboardName, bundle: nil).instantiateInitialViewController()
        }
        return nil
    }
    
    func viewController(viewControllerType: UIViewController.Type, from container: UIViewController) -> UIViewController? {
        guard container.children.count > 0 else { return nil }
        return container.children.filter {
            type(of: $0) == viewControllerType
        }
        .first
    }
}

