//
//  DecoderService.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

class FormatsService {
    var defaultJSONDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        return decoder
    }()
    
    var defaultDateJSONDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }()
    
    var defaultDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy"
        return formatter
    }()
    
    var defaultTimeFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.unitsStyle = .abbreviated
        return formatter
    }()
    //this varible should be accessible to change either in application settings or in configs from server
    let cacheLimitTime: TimeInterval = 60 * 60 * 24
}
