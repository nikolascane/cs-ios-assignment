//
//  ServiceManager.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

class ServiceManager {
    static let shared = ServiceManager()
    
    let formatService = FormatsService()
    let movieService = MovieService()
    let routerService = RouterService()
}
