//
//  RouterService.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

final class RouterService {
    // Create array of concrete routers to use it on each "handle(action:)" invoke
    // There is possibility to get all classes which conform to current ptotocol in runtime but usage of Swift protocols makes it hard to implement
    var routers: [RouterProtocol] = [MoviesRouter()]
    
    @discardableResult
    func handleConfig(action: RouteAction, for viewController: UIViewController) -> Bool {
        return self.routers
            .filter{$0.canConfig(action: action)}
            .first?
            .config(viewController: viewController, action: action) ?? false
    }
    
    func handleCreate(action: RouteAction) throws {
        if action.route?.path == .back {
            self.dismissViewTopViewContoller()
        }
        let vc = self.routers
            .filter({ $0.canRoute(action: action) })
            .first?
            .createViewController(action: action)
        guard let viewController = vc else { throw(NavigationError.cannotPresent(action.presentationMode, "View controller to present is nil")) }
        try self.route(viewController: viewController, mode: action.presentationMode)
    }
    
    func handleError(_ error: Error) throws {
        let topViewController = try self.topViewController()
        topViewController?.presentError(error)
    }
}

extension RouterService {
    private func route(viewController: UIViewController, mode: PresentationMode) throws {
        switch mode {
        case .push:
            if viewController is UINavigationController {
                throw(NavigationError.cannotPresent(mode, "UINavigationController cannot be pushed to navigation controller stack"))
            }
            try self.topViewController()?.show(viewController, sender: nil)
        case .modal:
            try self.topViewController()?.showDetailViewController(viewController, sender: nil)
        case .modalTransparent:
            viewController.modalPresentationStyle = .overCurrentContext
            try self.topViewController()?.showDetailViewController(viewController, sender: nil)
        case .none: ()
        }
    }
    
    // This method has basic functionality
    // Here intentionally was not handled cases with container view controllers which can contain current topmost view controller
    func topViewController() throws -> UIViewController? {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        guard var topController = keyWindow?.rootViewController else { throw(NavigationError.sourceError) }
    
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
        return topController
    }
    
    private func dismissViewTopViewContoller() {
        do {
            try self.topViewController()?.dismiss(animated: true, completion: nil)
        } catch let error {
            print(error)
        }
    }
}
