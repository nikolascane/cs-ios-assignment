//
//  Movie.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct MovieResponse: Decodable {
    let results: [Movie]?
    
    enum CodingKeys: String, CodingKey {
        case results = "results"
    }
    
    init(decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let moviesContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .results)
        self.results = try moviesContainer.decode([Movie].self, forKey: .results)
    }
}

class Movie: Decodable {
    let adult: Bool?
    let budget: Int?
    var genres: [Genre]?
    let homePage: String?
    let id: Int?
    let imdbId: String?
    let originalLanguage: String?
    let originalTitle: String?
    let overview: String?
    let popularity: Double?
    let posterPath: String?
    let releaseDate: Date?
    var releaseDateString: String = ""
    let revenue: Int?
    let runtime: Int?
    var runtimeString: String = ""
    let status: String?
    let tagline: String?
    let title: String?
    let voteAverage: Double?
    var rating: Int = 0
    let voteCount: Int?
    var smallPosterData: Data?
    var bigPosterData: Data?
    var smallPosterDataLoading: Bool = false
    var bigPosterDataLoading: Bool = false
    
    private enum CodingKeys: String, CodingKey {
        case adult = "adult"
        case budget = "budget"
        case genres = "genres"
        case homePage = "homepage"
        case id = "id"
        case imdbId = "imdb_id"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview = "overview"
        case popularity = "popularity"
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case revenue = "revenue"
        case runtime = "runtime"
        case status = "status"
        case tagline = "tagline"
        case title = "title"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
    
    init(decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let genresContaner = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .genres)
        
        self.adult = try container.decode(Bool.self, forKey: .adult)
        self.budget = try container.decode(Int.self, forKey: .budget)
        self.genres = try genresContaner.decode([Genre].self, forKey: .genres)
        self.homePage = try container.decode(String.self, forKey: .homePage)
        self.id = try container.decode(Int.self, forKey: .id)
        self.imdbId = try container.decode(String.self, forKey: .imdbId)
        self.originalLanguage = try container.decode(String.self, forKey: .originalLanguage)
        self.originalTitle = try container.decode(String.self, forKey: .originalTitle)
        self.overview = try container.decode(String.self, forKey: .overview)
        self.popularity = try container.decode(Double.self, forKey: .popularity)
        self.posterPath = try container.decode(String.self, forKey: .posterPath)
        self.releaseDate = try container.decode(Date.self, forKey: .releaseDate)
        self.revenue = try container.decode(Int.self, forKey: .revenue)
        self.runtime = try container.decode(Int.self, forKey: .runtime)
        self.status = try container.decode(String.self, forKey: .status)
        self.tagline = try container.decode(String.self, forKey: .tagline)
        self.title = try container.decode(String.self, forKey: .title)
        self.voteAverage = try container.decode(Double.self, forKey: .voteAverage)
        self.voteCount = try container.decode(Int.self, forKey: .voteCount)
    }
    
    struct Genre: Decodable {
        let id: Int?
        var name: String?
        
        enum CodingKeys: String, CodingKey {
            case id = "id"
            case name = "name"
        }
        
        init(decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id = try container.decode(Int.self, forKey: .id)
            self.name = try container.decode(String.self, forKey: .name)
        }
    }
}
