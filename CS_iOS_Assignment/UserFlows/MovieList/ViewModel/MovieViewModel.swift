//
//  MovieViewModel.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class MovieViewModel: MovieViewModelConfigProtocol {
    //MARK: MovieViewModelConfig
    weak var nowPlayingDelegate: NowPlayingMoviesViewModelProtocol?
    weak var movieListDelegate: MovieListViewModelProtocol?
    weak var movieDetailsDelegate: MovieDetailsViewModelProtocol?
    //MARK: MovieListProtocol variables
    var popularMovieList = [Movie]()
    //MARK: NowPlayingMoviesProtocol variables
    var nowPlayingMovieList = [Movie]()
    //MARK: MovieDetailsProtocol variables
    var currentMovie: Movie?
    //MARK: Local variables
    private var currentPage = 1
    
}

//MARK: MovieListProtocol
extension MovieViewModel: MovieListProtocol {
    func getMovieList() {
        MovieListWorker().perform(request: self.movieListRequest()) { [weak self] (result: Result<[Movie], NetworkError>) in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case .success(let movies):
                    let fromScratch = self.popularMovieList.isEmpty
                    self.popularMovieList.append(contentsOf: self.prettify(movies: movies))
                    self.currentPage += 1
                    let indexPaths = self.calculateIndexPaths(for: movies.count, oldMoviesCount: self.popularMovieList.count)
                    self.movieListDelegate?.reloadNewCellsFor(indexPaths: indexPaths, refresh: fromScratch)
                case .failure(let error):
                    self.presentError(error)
                }
            }
        }
    }
    
    private func prettify(movies: [Movie]) -> [Movie] {
        return movies.map{
            self.prettify(movie: $0)
        }
    }
    
    private func prettify(movie: Movie) -> Movie {
        if let releaseDate = movie.releaseDate {
            movie.releaseDateString = ServiceManager.shared.formatService.defaultDateFormatter.string(from: releaseDate)
        }
        if let runtime = movie.runtime  {
            movie.runtimeString = ServiceManager.shared.formatService.defaultTimeFormatter.string(from: TimeInterval(TimeInterval(runtime) * 60)) ?? ""
        }
        if let vote = movie.voteAverage {
            movie.rating = Int(vote * Double(10))
        }
        if let genres = movie.genres {
            movie.genres = genres.map{
                var genre = $0
                genre.name = $0.name?.uppercased()
                return genre
            }
        }
        return movie
    }
    
    private func calculateIndexPaths(for newMoviesCount: Int, oldMoviesCount: Int) -> [IndexPath] {
        let countBefore = oldMoviesCount - newMoviesCount
        let countAfter = oldMoviesCount
        
        return (countBefore..<countAfter).enumerated().map{
          IndexPath(row: $0.element, section: 0)
        }
    }
    
    func getNowPlayingMovies() {
        let nowPlayingRequest = self.nowPlayingRequest()
        MovieListWorker().perform(request: nowPlayingRequest) { [weak self] (result: Result<[Movie], NetworkError>) in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case .success(let movies):
                    let fromScratch = self.nowPlayingMovieList.isEmpty
                    self.nowPlayingMovieList.append(contentsOf: movies)
                    let indexPaths = self.calculateIndexPaths(for: movies.count, oldMoviesCount: self.nowPlayingMovieList.count)
                    self.nowPlayingDelegate?.reloadNewCellsFor(indexPaths: indexPaths, refresh: fromScratch)
                case .failure(let error):
                    self.presentError(error)
                }
            }
        }
    }

    func loadPopularMiniPoster(indexPath: IndexPath) {
        guard self.needLoadPoster(movie: self.popularMovieList[indexPath.row], big: false) else { return }
        self.popularMovieList[indexPath.row].smallPosterDataLoading = true
        self.loadPoster(for: self.popularMovieList[indexPath.row], size: .thumb) { [unowned self] data in
            self.popularMovieList[indexPath.row].smallPosterData = data
            self.popularMovieList[indexPath.row].smallPosterDataLoading = false
            self.movieListDelegate?.reloadOldCellsFor(indexPath: indexPath)
        }
    }
    
    func selectPopularMovie(indexPath: IndexPath) {
        self.movieListDelegate?.startActivity()
        self.loadMovie(for: indexPath, in: self.popularMovieList) { [unowned self] movie in
            self.movieListDelegate?.stopActivity()
            self.currentMovie = movie
            self.currentMovie?.smallPosterData = self.popularMovieList[indexPath.row].smallPosterData
            self.routeToDetailScreen()
        }
    }
     
    func configForNowPlayingController(nowPlayingDelegate: UIViewController) {
        let action = RouteAction(config: self, route: nil, configPath: .nowPlaying, presentationMode: .none)
        ServiceManager.shared.routerService.handleConfig(action: action, for: nowPlayingDelegate)
    }
}

//MARK: Requests
extension MovieViewModel {
    private func movieListRequest() -> MovieListRequest {
        return MovieListRequest(page: self.currentPage, language: .english)
    }
    
    private func posterRequest(size: PosterSize, path: String?) -> PosterRequest? {
        if let path = path {
            return PosterRequest(path: .poster(size: size, id: path))
        }
        return nil
    }
    
    private func nowPlayingRequest() -> NowPlayingRequest {
        NowPlayingRequest(language: .english)
    }
    
    private func movieRequest(id: Int) -> MovieRequest {
        MovieRequest(movieId: id)
    }
}

//MARK: MovieDetailsProtocol
extension MovieViewModel: MovieDetailsProtocol {
    func handleCloseButtonTap() {
        let action = RouteAction(config: nil, route: Route(path: .back), configPath: nil, presentationMode: .none)
        do {
            try ServiceManager.shared.routerService.handleCreate(action: action)
        } catch let error {
            self.presentError(error)
        }
    }
    
    func loadOriginalPoster(movie: Movie) {
        self.loadPoster(for: movie, size: .big, success: { [unowned self] data in
            self.movieDetailsDelegate?.reloadImageWithPoster(data: data)
        })
    }
}

//MARK: NowPlayingMoviesProtocol
extension MovieViewModel: NowPlayingMoviesProtocol {
    func loadNowPlayingMiniPoster(indexPath: IndexPath) {
        guard self.needLoadPoster(movie: self.nowPlayingMovieList[indexPath.row], big: false) else { return }
        self.nowPlayingMovieList[indexPath.row].smallPosterDataLoading = true
        self.loadPoster(for: self.nowPlayingMovieList[indexPath.row], size: .thumb) { [unowned self] data in
            self.nowPlayingMovieList[indexPath.row].smallPosterData = data
            self.nowPlayingMovieList[indexPath.row].smallPosterDataLoading = false
            self.nowPlayingDelegate?.reloadOldCellsFor(indexPath: indexPath)
        }
    }
    
    func selectNowPlayingMovie(indexPath: IndexPath) {
        self.movieListDelegate?.startActivity()
        self.loadMovie(for: indexPath, in: self.nowPlayingMovieList) { [unowned self] movie in
            self.movieListDelegate?.stopActivity()
            self.currentMovie = movie
            self.currentMovie?.smallPosterData = self.nowPlayingMovieList[indexPath.row].smallPosterData
            self.routeToDetailScreen()
        }
    }
}

//MARK: Helpers
extension MovieViewModel {
    private func loadMovie(for indexPath: IndexPath, in movieCollection: [Movie], ready: @escaping (Movie)->()) {
        guard let id = movieCollection[indexPath.row].id else { return }
        let request = self.movieRequest(id: id)
        MovieWorker().perform(request: request) { [weak self] (result: Result<Movie, NetworkError>) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case .success(let movie):
                    ready(self.prettify(movie: movie))
                case .failure(let error):
                    self.presentError(error)
                }
            }
        }
    }
    
    private func routeToDetailScreen() {
        do {
            let route = Route(path: .movieDetails)
            let action = RouteAction(config: self,
                                     route: route,
                                     configPath: nil,
                                     presentationMode: .modalTransparent)
            try ServiceManager.shared.routerService.handleCreate(action: action)
        }
        catch let error {
            self.presentError(error)
        }
    }
    
    private func loadPoster(for movie: Movie, size: PosterSize, success: @escaping (Data)->()) {
        guard let posterRequest = self.posterRequest(size: size, path: movie.posterPath) else { return }
        PosterWorker().perform(request: posterRequest) { [weak self] (result: Result<Data, NetworkError>) in
            DispatchQueue.main.async {
                switch result {
                case .success(let data):
                  success(data)
                case .failure(let error):
                    self?.presentError(error)
                }
            }
        }
    }
    
    private func needLoadPoster(movie: Movie, big: Bool) -> Bool {
      let decisionBlock = { (posterLoaded: Bool, posterLoading: Bool) -> Bool in
        if posterLoaded || posterLoading {
          return false
        }
        return true
      }
      if big {
        return decisionBlock(movie.bigPosterData != nil, movie.bigPosterDataLoading)
      }
      return decisionBlock(movie.smallPosterData != nil, movie.smallPosterDataLoading)
    }
    
    private func presentError(_ error: Error) {
        do {
            try ServiceManager.shared.routerService.handleError(error)
        }
        catch let presentingError {
            print(presentingError )
        }
    }
}
 
//MARK: Cache handlers
extension MovieViewModel {
    func checkCache() {
        if CacheManager.refreshAppCacheIfNeeded() {
            self.popularMovieList.removeAll()
            self.nowPlayingMovieList.removeAll()
        }
        if self.popularMovieList.isEmpty {
            self.getMovieList()
        }
        if self.nowPlayingMovieList.isEmpty {
            self.getNowPlayingMovies()
        }
    }
}
    
