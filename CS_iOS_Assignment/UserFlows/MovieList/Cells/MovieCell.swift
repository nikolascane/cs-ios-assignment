//
//  MovieCell.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

//
// MARK: - Movie Cell
//
class MovieCell: UITableViewCell {
    
    //
    // MARK: - Class Constants
    //
    static let identifier = "MovieCell"
    
    //
    // MARK: - IBOutlets
    //
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rating: RatingView!
    @IBOutlet weak var releaseDate: UILabel!
    // runtime is not presented in movie list, hence i didn't implement seperate fetching for this component because it'll cause a lot of additional requests.
    // its better to implement this filed on backend than unnecary load mobile client
    @IBOutlet weak var runtime: UILabel!
    @IBOutlet weak var poster: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.title.text = ""
        self.rating.clear()
        self.releaseDate.text = ""
        self.runtime.text = ""
        self.poster.image = #imageLiteral(resourceName: "posterPlaceholder")
    }
    
    func configure(movie: Movie) {
        self.title.text = movie.title
        self.releaseDate.text = movie.releaseDateString
        self.title.text = movie.title
        self.releaseDate.text = movie.releaseDateString
        self.runtime.text = movie.runtimeString
        if let smallPosterData = movie.smallPosterData {
            self.poster.image = UIImage(data: smallPosterData)
        }
        else {
            self.poster.image = #imageLiteral(resourceName: "posterPlaceholder")
        }
        self.rating.draw(rating: movie.rating)
    }
}
