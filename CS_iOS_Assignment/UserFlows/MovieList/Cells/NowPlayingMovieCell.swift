//
//  NowPlayingMovieCell.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

class NowPlayingMovieCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.movieImage.image = nil
    }
    
    func config(movie: Movie) {
        if let smallPosterData = movie.smallPosterData {
            self.movieImage.image = UIImage(data: smallPosterData )
        }
        else {
            self.movieImage.image = nil
        }
        
    }
    
}
