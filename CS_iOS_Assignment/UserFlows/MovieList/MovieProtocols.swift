//
//  MoviewProtocols.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

//MARK: ViewModel protocols
protocol MovieViewModelConfigProtocol: class {
    var nowPlayingDelegate: NowPlayingMoviesViewModelProtocol? {get set}
    var movieListDelegate: MovieListViewModelProtocol? {get set}
    var movieDetailsDelegate: MovieDetailsViewModelProtocol? {get set}
}

protocol MovieListProtocol: class {
    var popularMovieList: [Movie] {get set}
    
    func getMovieList()
    func checkCache()
    func loadPopularMiniPoster(indexPath: IndexPath)
    func selectPopularMovie(indexPath: IndexPath)
    func configForNowPlayingController(nowPlayingDelegate: UIViewController)
}

protocol MovieDetailsProtocol: class {
    var currentMovie: Movie? {get set}
    
    func loadOriginalPoster(movie: Movie)
    func handleCloseButtonTap()
}

protocol NowPlayingMoviesProtocol: class {
    var nowPlayingMovieList: [Movie] {get set}
    
    func loadNowPlayingMiniPoster(indexPath: IndexPath)
    func selectNowPlayingMovie(indexPath: IndexPath)
}

//MARK: ViewController protocols
protocol MovieListViewModelProtocol: class {
    func config(viewModel: MovieListProtocol)
    func reloadNewCellsFor(indexPaths: [IndexPath], refresh: Bool)
    func reloadOldCellsFor(indexPath: IndexPath)
    func startActivity()
    func stopActivity()
}

protocol MovieDetailsViewModelProtocol: class {
    func reloadImageWithPoster(data: Data) 
}

protocol NowPlayingMoviesViewModelProtocol: class {
    func config(viewModel: NowPlayingMoviesProtocol)
    func reloadNewCellsFor(indexPaths: [IndexPath], refresh: Bool)
    func reloadOldCellsFor(indexPath: IndexPath)
}
