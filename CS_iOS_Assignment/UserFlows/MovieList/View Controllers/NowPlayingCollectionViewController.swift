//
//  NowPlayingCollectionViewController.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit



class NowPlayingCollectionViewController: UICollectionViewController {
    // Force unwrap used intentionally. ViewModel is vital part of ViewController and without it there is no need in ViewController at all
    private weak var viewModel: NowPlayingMoviesProtocol!
    
    func config(viewModel: NowPlayingMoviesProtocol) {
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.nowPlayingMovieList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NowPlayingMovieCell.cellID, for: indexPath) as? NowPlayingMovieCell {
            cell.config(movie:self.viewModel.nowPlayingMovieList[indexPath.row])
            cell.accessibilityIdentifier = "Now Playing Movies CollectionView Cell \(indexPath.item)"
            self.viewModel.loadNowPlayingMiniPoster(indexPath: indexPath)
            return cell
        }
        return UICollectionViewCell()
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        self.viewModel.selectNowPlayingMovie(indexPath: indexPath)
    }
}

extension NowPlayingCollectionViewController: NowPlayingMoviesViewModelProtocol {
    func reloadNewCellsFor(indexPaths: [IndexPath], refresh: Bool) {
        if refresh {
            self.collectionView.reloadData()
            return 
        }
        self.collectionView.performBatchUpdates({
            self.collectionView.insertItems(at: indexPaths)
        }) { finished in }
    }
    
    func reloadOldCellsFor(indexPath: IndexPath) {
        self.collectionView.reloadItems(at: [indexPath])
    }
}
