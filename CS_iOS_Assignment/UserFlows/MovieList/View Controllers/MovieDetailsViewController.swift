//
//  MoviewDetailsViewController.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class MovieDetailsViewController: UIViewController {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var releaseDate_runtimeLabel: UILabel!
    @IBOutlet weak var overviewText: UILabel!
    @IBOutlet weak var tagView: TagViewContainer!
    
    // Force unwrap used intentionally. ViewModel is vital part of ViewController and without it there is no need in ViewController at all
    private weak var viewModel: MovieDetailsProtocol!

    func config(viewModel: MovieDetailsProtocol) {
        self.viewModel = viewModel
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let currentMovie = self.viewModel.currentMovie {
            self.viewModel.loadOriginalPoster(movie: currentMovie)
        }
        
    }
    
    private func configViews() {
        guard let currentMovie = self.viewModel.currentMovie else { return }
        
        self.movieName.text = currentMovie.title
        self.releaseDate_runtimeLabel.text = "\(currentMovie.releaseDateString) - \(currentMovie.runtimeString)"
        self.overviewText.text = currentMovie.overview
        
        if let posterData = currentMovie.smallPosterData {
            self.posterImageView.image = UIImage(data: posterData)
        }
        if let genres = currentMovie.genres {
            self.tagView.render(tags: genres)
        }
    }
}

//MARK: Actions
extension MovieDetailsViewController {
    @IBAction func closeButtonDidTap(_ sender: Any) {
        self.viewModel.handleCloseButtonTap()
    }
}

extension MovieDetailsViewController: MovieDetailsViewModelProtocol {
    func reloadImageWithPoster(data: Data) {
        self.posterImageView.image = UIImage(data: data)
    }
}
