//
//  ViewController.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

private let cellHeight: CGFloat = 93

class MovieListViewController: UIViewController {
    
    @IBOutlet weak var moviesTableView: UITableView!
    @IBOutlet weak var nowPlayingHeaderTitle: UILabel!
    @IBOutlet weak var mostPopularHeaderTitle: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // Force unwrep used here because child view controller is much like IBOutlet
    private unowned var nowPlayingCollectionViewController: NowPlayingCollectionViewController!
    
    // Force unwrap used intentionally. ViewModel is vital part of ViewController and without it there is no need in ViewController at all
    private var viewModel: MovieListProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupContent()
        self.setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.checkCache()
    }
    
    private func setupContent() {
        // Again force unwrapping, because this component is expected to be in view stack
        self.nowPlayingCollectionViewController = self.children.first as? NowPlayingCollectionViewController
        self.viewModel.configForNowPlayingController(nowPlayingDelegate: self.nowPlayingCollectionViewController)
    }
    
    private func setupViews() {
        self.stopActivity()
    }
}

extension MovieListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.popularMovieList.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.dequeueReusableCell(withIdentifier: MovieCell.cellID, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? MovieCell {
            cell.configure(movie: self.viewModel.popularMovieList[indexPath.row])
            self.viewModel.loadPopularMiniPoster(indexPath: indexPath)
            cell.accessibilityIdentifier = "Movie TableView List Cell \(indexPath.row)"
        }
        
    }
}

extension MovieListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.viewModel.selectPopularMovie(indexPath: indexPath)
    }
}

extension MovieListViewController: MovieListViewModelProtocol {
    func config(viewModel: MovieListProtocol) {
        self.viewModel = viewModel
    }
    
    func reloadNewCellsFor(indexPaths: [IndexPath], refresh: Bool) {
        if refresh {
            self.moviesTableView.reloadData()
            return 
        }
        self.moviesTableView.beginUpdates()
        self.moviesTableView.insertRows(at: indexPaths, with: .none)
        self.moviesTableView.endUpdates()
    }
    
    func reloadOldCellsFor(indexPath: IndexPath) {
        if let visiblePaths = self.moviesTableView.indexPathsForVisibleRows, visiblePaths.contains(indexPath) {
            self.moviesTableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    func startActivity() {
        self.activityIndicator.startAnimating()
    }
    
    func stopActivity() {
        self.activityIndicator.stopAnimating()
    }
}

extension MovieListViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let scrollHeigh = scrollView.frame.height
        if offsetY < 0 || contentHeight < cellHeight {
            return
        }
        if (offsetY + scrollHeigh >= contentHeight - cellHeight) {
            self.viewModel.getMovieList()
        }
    }
}

