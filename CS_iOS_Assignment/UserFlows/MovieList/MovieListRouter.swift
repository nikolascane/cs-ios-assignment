//
//  MovieListRouter.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class MoviesRouter: RouterProtocol {
    let routes: [Route.Path] = [.movieDetails]
    let configs: [ConfigPath] = [.movieList, .nowPlaying]
    
    func typeOf(path: ConfigPath) -> UIViewController.Type {
        switch path {
        case .movieList:
            return MovieListViewController.self
        case .nowPlaying:
            return NowPlayingCollectionViewController.self
        case .movieDetails:
            return MovieDetailsViewController.self
        default:
            return UIViewController.self
        }
    }
    
    var currentStoryboardName: String = {
       "Main"
    }()
    
    func createViewController(action: RouteAction) -> UIViewController? {
        guard var route = action.route else { return nil }
        switch route.path {
        case .movieDetails:
            route.screenName = MovieDetailsViewController.description().components(separatedBy: ".").last
            route.storyboardName = self.currentStoryboardName
            if let viewController = self.instantiateViewController(route: route) as? MovieDetailsViewController,
                let viewModel = action.config as? MovieDetailsProtocol & MovieViewModelConfigProtocol {
                viewController.config(viewModel: viewModel)
                viewModel.movieDetailsDelegate = viewController
                return viewController
            }
        default:
            return nil
        }
        return nil
    }
    
    func config(viewController: UIViewController, action: RouteAction) -> Bool {
        let viewControllerType = type(of: viewController)
        switch action.configPath {
        case .movieList:
            var movieList: MovieListViewController?
            if viewControllerType != self.typeOf(path: action.configPath ?? .none) {
                movieList = self.viewController(viewControllerType: MovieListViewController.self, from: viewController) as? MovieListViewController 
            }
            if let movieList = movieList,
                let viewModel = action.config as? MovieListProtocol & MovieViewModelConfigProtocol {
                let titleImageView = UIImageView(image: UIImage(named: "titleImage"))
                titleImageView.contentMode = .scaleAspectFit
                movieList.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background"), for: UIBarMetrics(rawValue: 0)!)
                movieList.navigationItem.titleView = titleImageView
                movieList.config(viewModel: viewModel)
                viewModel.movieListDelegate = movieList
                return true
            }
            return false
        case .nowPlaying:
            if let nowPlaying = viewController as? NowPlayingMoviesViewModelProtocol,
                let viewModel = action.config as? NowPlayingMoviesProtocol & MovieViewModelConfigProtocol {
                nowPlaying.config(viewModel: viewModel)
                viewModel.nowPlayingDelegate = nowPlaying
                return true
            }
        default:
            return false
        }
        return false
    }
}
