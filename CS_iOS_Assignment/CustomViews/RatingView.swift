//
//  RatingView.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//
import UIKit
import Foundation

private let redRange = (0..<0.33)
private let yellowRange = (0.33..<0.66)
private let greenRange = (0.66...1)

class RatingView: UIView {
    var path: UIBezierPath!
    
    private func ratingColor(rating: CGFloat) -> UIColor {
        if redRange.contains(Double(rating)) {
            return UIColor.red
        }
        else if yellowRange.contains(Double(rating)) {
            return UIColor.yellowRating
        }
        return UIColor.greenRating
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initSubviews()
    }
    
    func draw(rating: Int) {
        self.drawCircle(rating: CGFloat(rating) / CGFloat(100))
        self.drawNumbers(rating: rating)
    }
    
    private func drawCircle(rating: CGFloat) {
        let frame = self.bounds
        let path = UIBezierPath(arcCenter: CGPoint(x: frame.width / 2, y: frame.height / 2),
                                radius: frame.size.width / 2,
                                startAngle: -CGFloat.pi/2,
                                endAngle: CGFloat.pi*3/2,
                                clockwise: true)

        let circle = CAShapeLayer()
        circle.path = path.cgPath
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = self.ratingColor(rating: rating).cgColor
        circle.lineWidth = 5.0
        circle.lineCap = .round
        circle.strokeEnd = rating
        self.layer.addSublayer(circle)
    }
    
    private func drawNumbers(rating: Int) {
        let frame = self.bounds
        let fontSize: CGFloat = 16
        let percentFontSize: CGFloat = 8
        let yOffset = (frame.height - fontSize) / 2 - 2
        let xOffset: CGFloat = 5
        let percentXOffset = frame.width / CGFloat(3) * CGFloat(2)
        let percentYOffset = yOffset + 2
        let width = frame.width - xOffset
        let percentWidth: CGFloat = 7
        
        let number = CATextLayer()
        number.frame = CGRect(x: 0, y: yOffset, width: width, height: fontSize)
        number.string = "\(rating)"
        number.foregroundColor = UIColor.cellTitle.cgColor
        number.fontSize = fontSize
        number.alignmentMode = .center
        number.contentsScale = UIScreen.main.scale
        
        let percent = CATextLayer()
        percent.frame = CGRect(x: percentXOffset, y: percentYOffset, width: percentWidth, height: percentFontSize)
        percent.string = "%"
        percent.foregroundColor = UIColor.cellTitle.cgColor
        percent.fontSize = percentFontSize
        percent.alignmentMode = .center
        percent.contentsScale = UIScreen.main.scale
        
        self.layer.addSublayer(number)
        self.layer.addSublayer(percent)
    }
    
    func initSubviews() {
        self.backgroundColor = UIColor.clear
    }
    
    func clear() {
        self.layer.sublayers?.forEach{
            $0.removeFromSuperlayer()
        }
    }
}
