//
//  TagViewContainer.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 28.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

private let stackCapacity = 3

class TagViewContainer: UIView {
    
    @IBOutlet var stackViews: [UIStackView]!
    
    func render(tags: [Movie.Genre]) {
        tags.enumerated().forEach { tag in
            let stackIndex: Int = tag.offset / 3
            let view = self.viewWith(title: tag.element.name)
            self.stackViews[stackIndex].addArrangedSubview(view)
        }
    }
    
    private func viewWith(title: String?) -> UIView {
        let view = UIView()
        view.layer.cornerRadius = 4
        view.backgroundColor = UIColor.white
        self.addSubview(view)
        
        let label = UILabel()
        view.addSubview(label)
        
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = title
        label.textColor = UIColor.black
        label.backgroundColor = UIColor.clear
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        
        let constraints = [label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
          label.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8),
          label.heightAnchor.constraint(equalToConstant: 14),
          label.centerYAnchor.constraint(equalTo: view.centerYAnchor)]
        NSLayoutConstraint.activate(constraints)
        
        return view
    }

}

