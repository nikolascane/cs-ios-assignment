//
//  README.md
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 01.05.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

1.  Router instance was used because it bears navigation layer separately. It uses list of concrete routers for every user flow. Every concrete router knows about their own config paths & routing paths. If screen assumes both to be used as is like MivieListViewController and to be creates later like MovieDetailsViewController then config paths & routing paths can have intersecting elements. And config would just complement create method. SceneDelegate uses as entry point to config everything as a logical entry point.
2.   MovieCell contains runtime label but it doesn't contains text, because on movie list fetched from backend, hence i didn't implement seperate fetching for this component because it'll cause a lot of additional requests. Its better to implement this filed on backend than unnecary load mobile client
3.   For RatingView CALayer drawing was used as the fastest working approach. It allows to create drawing on the fly without loading GPU more than possible to render cells on time.
4.  For horizontal movie list UICollectionView was used as most simple and productive approach. It wasn't embedded in UITableViewHeaderFooterView or UITableViewCell for a reason that I intended to use separate UICollectioViewController to display "Now playing movies" and it's nort allowed to use container in reusable elements. It was implemented as a part of UITableView to natively scroll along with "Popular movies". 
5.  MovieViewModel conforms to several protocol to separate methods and properies, accessible to view controllers. Every view controller also hidden behind protocol and cand be replaced if needed.
6.  Caching was implemented with embedded FileManager. For caching seperate manager was created. It is harcoded value (1 day) to clear cache. It should be included as a setting on a settings page or should received fron backend as as config in real application. Cache was implemented in separate subfolder of Documents folder to easily remove it without iterating over files and to not load device. When Cache directory removed it is created right away. Serial queue was used to make operation atomic. Storage was provided by StorageManager, which can create and remove paths and give them by request.
7.   "Movie" model used to contain all movie data. There were implemented helper fields to hold calculated in runtime data. It contains image data (not images) for the sake of memory saving. Creating small image on scrolling table view from Data is very easy task for modern devices. But rendered image in array could take unexpectedly large amount of memory. 
8.   Networking implemented via service "MovieService" which contains list of all current request to manage them. Writing to and reading from list implemented using serial queue to not crash app when sepatrate theads would want to change list. 
9.  All requests should conform to RequestProtocol. RequestProtocol implements extension to generate URL from given request. 
10. All request passed to "MovieService" using workers which conform to WorkerProtocol. WorkerProtocol designated for fetching data using URLs. It implements checking for cached data before pass requests to MovieService. It is possible to define another protocol which would implement different fetching options.
11.  For Reachability simple version of network checking was used.
12.  "Endpoints" contains set of data to combine in requests to URLs. Every request contains appropriate information. Logic for variable query items is inside view model.
13. "Download" class used to manage list of current downloads by MovieService
14. To implement custom colors, define cell identifiers, compose URL , present UIIAlertView class extensions were dreated.
15. ServiceManager implements service layer. It gives appropriate service to appropriate requester. It is shared instance because data it holds in services should be created once for applicaiton cycle.
16. Format service contains decoders and formatters which is too expensive to create over time.
17. Tags MovieDetailsViewController were implemented using UIStackView. After inerstigations It was detected that each row rarily contains more than 3 tags. And its enough for every modern screen to render 3 tags in line. Movie can't contain more than 9 tags. So using array UIStackViews with detached on runtime tails anchors needed behavior was achived.
18. For the testing purpose all approprialte views was assigned with accessibilityIdentifiers. Main idea of interfaces is to hide reusable components behind protocols. This approach is useful in testing. When testing all interfaces should be evaluated. Concrete implementation of components is not so important while it produces expectable results. To test behavior of view model complemented delegates were mocked.

