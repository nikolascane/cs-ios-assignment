//
//  TableViewCell+Id.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    static var cellID: String {
        return String(describing: "\(Self.self)Identifier")
    }
}

extension UICollectionViewCell {
    static var cellID: String {
        return String(describing: "\(Self.self)Identifier")
    }
}
