//
//  UIViewController+Alert.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 01.05.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

extension UIViewController {
  func presentError(_ error: Error) {
    self.presentAlert(title: "Error", message: error.localizedDescription)
  }
  
  func presentAlert(title: String,
                    message: String,
                    confirmAction: (()->())? = nil,
                    style: UIAlertController.Style = .alert) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: style)
    
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { [unowned alert] _ in
      alert.dismiss(animated: true, completion: nil)
    }))
    if let confirmAction = confirmAction {
      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [unowned alert] _ in
        confirmAction()
        alert.dismiss(animated: true, completion: nil)
      }))
    }
    self.present(alert, animated: true, completion: nil)
  }
}
