//
//  UIColor+defined.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 28.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

extension UIColor {
    static var cellTitle: UIColor {
        UIColor(displayP3Red: 0.878, green: 0.878, blue: 0.878, alpha: 1.0)
    }
    
    static var yellowRating: UIColor {
        UIColor(displayP3Red: 0.824, green: 0.84, blue: 0.188, alpha: 1.0)
    }
    
    static var greenRating: UIColor {
        UIColor(displayP3Red: 0.122, green: 0.84, blue: 0.486, alpha: 1.0)
    }
}

