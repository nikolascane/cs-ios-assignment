//
//  WorkerProtocol.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

protocol WorkerProtocol {
    associatedtype Object
    
    var decoder: JSONDecoder? {get}
    
    func perform<Requestable: RequestProtocol>(request: Requestable, resultCompletion: @escaping (Result<Object, NetworkError>)->())
    func objectFrom(data: Data) -> Result<Object, NetworkError>
}


extension WorkerProtocol {
    func provide<Requestable: RequestProtocol>(request: Requestable, resultCompletion: @escaping (Result<Object, NetworkError>) -> ()) {
        if let data = self.checkData(request: request) {
            resultCompletion(self.objectFrom(data: data))
            return
        }
        ServiceManager.shared.movieService.performRequest(request) { (result: Result<URL, NetworkError>) in
          switch result {
          case .success(let url):
            if let data = try? Data(contentsOf: url) {
              resultCompletion(self.objectFrom(data: data))
            }
          case .failure(let error):
            resultCompletion(Result.failure(error))
          }
        }
    }
    
    private func checkData(request: RequestProtocol) -> Data? {
        guard let url = request.url,
              let destinationURL = StorageManager.destinationURL(from: url) else {
                return nil
        }
        if let data = try? Data(contentsOf: destinationURL) {
           return data
        }
        return nil
    }
}

