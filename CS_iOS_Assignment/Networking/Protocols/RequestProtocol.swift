//
//  RequestProtocol.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

enum RequestMethod: String {
    case GET
    case POST
}

protocol RequestProtocol {
    var attempts: Int {get set}
    var apiKeyRequired: Bool {get}
    var timeout: TimeInterval {get}
    var method: RequestMethod {get}
    var queryItems: [String: String]? {get}
    var host: Host {get}
    var path: Path {get}
    var url: URL? {get}
}

extension RequestProtocol {
  
    var url: URL? { self.urlComponents.url }
  
    private var urlComponents: URLComponents {
        var components = URLComponents()
        components.scheme = "https"
        components.host = self.host.rawValue
        components.path = self.path.path
        if let queryItems = self.checkForApiKeyIfNeeded() {
            components.setQueryItems(with: queryItems)
        }
        return components
    }
    
    private func checkForApiKeyIfNeeded() -> [String: String]? {
        if self.apiKeyRequired {
            var checkedQueryItems = self.queryItems
            checkedQueryItems?[QueryItems.MovieDB.apiKey] = ApiKey.key
            return checkedQueryItems
        }
        else {
            return self.queryItems
        }
    }
}
