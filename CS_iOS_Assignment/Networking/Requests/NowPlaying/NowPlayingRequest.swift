//
//  NowPlayingRequest.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct NowPlayingRequest: RequestProtocol {
    var attempts = 3
    let apiKeyRequired = true
    let timeout = 30.0
    let method = RequestMethod.GET
    var queryItems: [String : String]? {
        [QueryItems.MovieDB.language: "\(self.language)"]
    }
    var host: Host = .movieDb
    var path: Path = .nowPlaying
    
    var language: Language
    //page variable was omitted here because it is optional, but requires additional locig what cal lead to tangled behaviour
}

