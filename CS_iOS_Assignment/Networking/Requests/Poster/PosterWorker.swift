//
//  PosterWorker.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct PosterWorker: WorkerProtocol {
    var decoder: JSONDecoder?
    
    func perform<Requestable>(request: Requestable, resultCompletion: @escaping (Result<Data, NetworkError>) -> ()) where Requestable : RequestProtocol {
        self.provide(request: request, resultCompletion: resultCompletion)
    }
    
    func objectFrom(data: Data) -> Result<Data, NetworkError> {
        return Result.success(data)
    }
}
