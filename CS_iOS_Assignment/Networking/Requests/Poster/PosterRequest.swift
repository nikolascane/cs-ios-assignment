//
//  PosterRequest.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct PosterRequest: RequestProtocol {
    var attempts = 3
    let apiKeyRequired = false
    let timeout = 30.0
    var method = RequestMethod.GET
    var queryItems: [String : String]?
    var host: Host = .imageDb
    var path: Path 
}
