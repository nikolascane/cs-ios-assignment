//
//  MovieListWorker.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct MovieListWorker: WorkerProtocol {
    var decoder: JSONDecoder? {
        ServiceManager.shared.formatService.defaultDateJSONDecoder
    }
    
    func perform<Requestable: RequestProtocol>(request: Requestable, resultCompletion: @escaping (Result<[Movie], NetworkError>) -> ()) {
        self.provide(request: request, resultCompletion: resultCompletion)
    }
    
    func objectFrom(data: Data) -> Result<[Movie], NetworkError> {
        do {
            guard let result = try self.decoder?.decode(MovieResponse.self, from: data),
                let movies = result.results else {
                return Result.failure(NetworkError.parseError("Unexpected parsing error"))
            }
            return Result.success(movies)
        }
        catch let error {
            return Result.failure(NetworkError.parseError(error.localizedDescription))
        }
    }
}
