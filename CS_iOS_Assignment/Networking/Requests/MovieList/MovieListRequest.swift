//
//  MovieListRequest.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct MovieListRequest: RequestProtocol {
    var attempts = 3
    let apiKeyRequired = true
    let timeout = 30.0
    let method = RequestMethod.GET
    var queryItems: [String : String]? {
        [QueryItems.MovieDB.language: self.language.rawValue,
         QueryItems.MovieDB.page: "\(self.page)"]
    }
    let host: Host = .movieDb
    let path: Path = .popular
    
    let page: Int
    let language: Language
}
