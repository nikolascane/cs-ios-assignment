//
//  MovieWorker.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct MovieWorker: WorkerProtocol {
    var decoder: JSONDecoder? {
        ServiceManager.shared.formatService.defaultDateJSONDecoder
    }
    
     func perform<Requestable>(request: Requestable, resultCompletion: @escaping (Result<Movie, NetworkError>) -> ()) where Requestable : RequestProtocol {
           self.provide(request: request, resultCompletion: resultCompletion)
       }
       
       func objectFrom(data: Data) -> Result<Movie, NetworkError> {
           do {
               guard let movie = try self.decoder?.decode(Movie.self, from: data) else {
                   return Result.failure(NetworkError.parseError("Unexpected parsing error"))
               }
               return Result.success(movie)
           }
           catch let error {
               return Result.failure(NetworkError.parseError(error.localizedDescription))
           }
       }
}
