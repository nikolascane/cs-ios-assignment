//
//  MovieRequest.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct MovieRequest: RequestProtocol {
    var attempts = 3
    let apiKeyRequired = true
    let timeout = 30.0
    let method = RequestMethod.GET
    var queryItems: [String : String]? = [:]
    var host = Host.movieDb
    var path: Path {
        .movie(id: self.movieId)
    }
    
    var movieId: Int
}
