//
//  Download.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

class Download {
    var resumeData: Data?
    var downloading: Bool = false
    var requestUrl: URL
    var task: URLSessionDataTask
    var finished: ((Result<URL, NetworkError>)->())?
    var request: RequestProtocol?
  
    init(requestUrl: URL,
         task: URLSessionDataTask) {
        self.requestUrl = requestUrl
        self.task = task
    }
  
    func repeatRequest() {
        self.task.cancel()
        self.task.resume()
    }
}
