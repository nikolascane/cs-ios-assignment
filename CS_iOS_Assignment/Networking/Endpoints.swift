//
//  Endpoints.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 27.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

// Two similar key used here for simplicity. In real-world application for non-debug config it would be used production key
struct ApiKey {
    #if DEBUG
    static let key = "55957fcf3ba81b137f8fc01ac5a31fb5"
    #else
    static let key = "55957fcf3ba81b137f8fc01ac5a31fb5"
    #endif
}

struct QueryItems {
    struct MovieDB {
        static let apiKey = "api_key"
        static let language = "language"
        static let page = "page"
    }
}

enum Host: String {
    case movieDb = "api.themoviedb.org"
    case imageDb = "image.tmdb.org"
}

enum PosterSize: String {
    case thumb = "w200"
    case big = "original"
}

enum Language: String {
    case english = "en-US"
    case russian = "ru=RU"
    case ukrainian = "uk-UA"
}

enum Path {
    case popular
    case nowPlaying
    case movie(id: Int)
    case poster(size: PosterSize, id: String)
    
    var path: String {
        switch self {
        case .popular:
            return "/3/movie/popular"
        case .nowPlaying:
            return "/3/movie/now_playing"
        case .movie(id: let id):
            return "/3/movie/\(id)"
        case .poster(size: let size, id: let id):
            return "/t/p/\(size.rawValue)\(id)"
        }
    }
}
