//
//  MovieServiceTest.swift
//  CS_iOS_AssignmentTests
//
//  Created by Nik Cane on 30.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import XCTest
@testable import CS_iOS_Assignment


class MovieViewModelTest: XCTestCase {
    
    var sut: MovieViewModel!
    var movieListDelegate: MovieListViewModelDelegateMock!
    var nowPlayingDelegate: NowPlayingViewModelDelegateMock!
    var movieDetailsDelegate: MovieDetailsViewModelDelegateMock!
    
    var defaultDateJSONDecoder: JSONDecoder!
    
    var singleMovie: Movie!
    var movieList: [Movie]!

    override func setUpWithError() throws {
        try super.setUpWithError()
        
        sut = MovieViewModel()
        movieListDelegate = MovieListViewModelDelegateMock()
        nowPlayingDelegate = NowPlayingViewModelDelegateMock()
        movieDetailsDelegate = MovieDetailsViewModelDelegateMock()
        sut.movieListDelegate = movieListDelegate
        sut.nowPlayingDelegate = nowPlayingDelegate
        sut.movieDetailsDelegate = movieDetailsDelegate
        
        instantiateJSONDecoder()
        instantiateSingleMovie()
        instantiateMovieList()
    }
    
    func instantiateJSONDecoder() {
        defaultDateJSONDecoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        defaultDateJSONDecoder.dateDecodingStrategy = .formatted(dateFormatter)
    }
    
    func instantiateSingleMovie() {
        let testBundle = Bundle(for: type(of: self))
        let singleMoviePath = testBundle.path(forResource: "SingleMovie", ofType: "JSON")
        let singleMovieData = try? Data(contentsOf: URL(fileURLWithPath: singleMoviePath!), options: .alwaysMapped)
        singleMovie = try? defaultDateJSONDecoder.decode(Movie.self, from: singleMovieData!)
    }
    
    func instantiateMovieList() {
        let testBundle = Bundle(for: type(of: self))
        let moviesPath = testBundle.path(forResource: "MovieList", ofType: "JSON")
        let moviesData = try? Data(contentsOf: URL(fileURLWithPath: moviesPath!), options: .alwaysMapped)
        movieList = try? defaultDateJSONDecoder.decode([Movie].self, from: moviesData!)
    }
    
    override func tearDownWithError() throws {
        try super.tearDownWithError()
        sut = nil
        movieListDelegate = nil
        nowPlayingDelegate = nil
        movieDetailsDelegate = nil
        defaultDateJSONDecoder = nil
        singleMovie = nil
        movieList = nil
    }
    
    func test_MovieListProtocol_getMovieList() throws {
        let promise = expectation(description: "movie list loaded")
        sut.getMovieList()
        let timeout: TimeInterval = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        XCTAssert(movieListDelegate.newCallsIndexPaths?.count ?? 0 > 0, "After call of getMovieList method movieListDelegate should receive list of index paths to insert into table view")
        XCTAssert(sut.popularMovieList.count > 0, "number of elements in popularMovieList property should be positive")
    }

    func test_MovieListProtocol_loadPopularMiniPoster() throws {
        let promise = expectation(description: "mini poster loaded")
        let indexPath = IndexPath(row: 3, section: 0)
        
        sut.popularMovieList = movieList
        sut.loadPopularMiniPoster(indexPath: indexPath)
        let timeout: TimeInterval = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        XCTAssertEqual(movieListDelegate.oldCellIndexPath, indexPath, "Model should return same indexPath to reload cell")
    }
    
    func test_MovieListProtocol_selectPopularMovie() throws {
        let promise = expectation(description: "movie loaded")
        let indexPath = IndexPath(row: 11, section: 0)
        
        sut.popularMovieList = movieList
        sut.selectPopularMovie(indexPath: indexPath)
        
        let timeout: TimeInterval = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        
        XCTAssertNotNil(sut.currentMovie, "view model should assign loaded by index path movie to currentMovie property")
        XCTAssert(movieListDelegate.activityStarted, "Activity should start")
        XCTAssert(movieListDelegate.activityStopped, "Activity should stop")
    }
    
    func test_MovieDetailsProtocol_loadOriginalPoster() throws {
        let promise = expectation(description: "poster loaded")
        
        sut.loadOriginalPoster(movie: singleMovie)
        
        let timeout: TimeInterval = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        
        XCTAssertNotNil(movieDetailsDelegate.posterData, "Poster data should be presented")
    }
    
    func test_MovieDetailsProtocol_handleCloseButtonTap_successCase() {
        let presentPromise = expectation(description: "View controller presented")
        let dismissPromise = expectation(description: "View controller dismissed")
        let timeout: TimeInterval = 1
        let initialTopViewController = try! ServiceManager.shared.routerService.topViewController()
        initialTopViewController?.present(UIViewController(), animated: false, completion: nil)
        let presentedViewController = try! ServiceManager.shared.routerService.topViewController()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            presentPromise.fulfill()
        }
        wait(for: [presentPromise], timeout: timeout)
        
        XCTAssertNotEqual(initialTopViewController, presentedViewController, "After present action topmost view controller should be different")
        
        sut.handleCloseButtonTap()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            dismissPromise.fulfill()
        }
        wait(for: [dismissPromise], timeout: timeout)
        
        let remainedTopViewController = try! ServiceManager.shared.routerService.topViewController()
        XCTAssertEqual(initialTopViewController, remainedTopViewController, "After dismiss action topmost view controller should be same")
    }
    
    func test_MovieDetailsProtocol_SelectPopulatMovie_errorHandling() {
        let alertPromise = expectation(description: "Alert should be shown")
        let timeout: TimeInterval = 1
        
        ServiceManager.shared.routerService.routers.removeAll()
        
        sut.popularMovieList = movieList
        sut.selectPopularMovie(indexPath: IndexPath(row: 1, section: 0))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            alertPromise.fulfill()
        }
        wait(for: [alertPromise], timeout: timeout)
        
        let topViewController = try! ServiceManager.shared.routerService.topViewController()!
        XCTAssert(type(of: topViewController) == UIAlertController.self, "UIAlertController should be presented on error")
    }
    
    func test_NowPlayingMoviesProtocol_loadNowPlayingMiniPoster() {
        let promise = expectation(description: "poster loaded")
        let indexPath = IndexPath(row: 8, section: 0)
        
        sut.nowPlayingMovieList = movieList
        sut.loadNowPlayingMiniPoster(indexPath: indexPath)
        
        let timeout: TimeInterval = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        XCTAssertEqual(self.nowPlayingDelegate.oldCellIndexPath, indexPath, "After loading poster view model should send same index path to reload cell")
    }
    
    func test_NowPlayingMoviesProtocol_selectNowPlayingMovie() {
        let promise = expectation(description: "movie loaded")
        let indexPath = IndexPath(row: 12, section: 0)
        
        sut.nowPlayingMovieList = movieList
        sut.selectNowPlayingMovie(indexPath: indexPath)
        
        let timeout: TimeInterval = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        
        XCTAssertNotNil(sut.currentMovie, "view model should assign loaded by index path movie to currentMovie property")
        XCTAssert(movieListDelegate.activityStarted, "Activity should start")
        XCTAssert(movieListDelegate.activityStopped, "Activity should stop")
    }
    
    func testCacheManagement_firstAppLaunch() {
        StorageManager.clearCache()
        
        sut.checkCache()

        let promise = expectation(description: "movies loaded")
        let timeout: TimeInterval = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        
        XCTAssertFalse(sut.popularMovieList.isEmpty, "popularMovieList should load from scratch")
        XCTAssertFalse(sut.nowPlayingMovieList.isEmpty, "nowPlayingMovieList should load from scratch")
        XCTAssert(movieListDelegate.dataWasRefreshed, "New movies should be prompted to reload in table view")
        XCTAssert(nowPlayingDelegate.dataWasRefreshed, "New movies should be prompted to reload in collection view")
    }
    
    func testCacheManagement_appLaunchBeforeCacheObsoletes() {
        sut.popularMovieList.append(contentsOf: movieList)
        sut.popularMovieList.append(contentsOf: movieList)
        sut.nowPlayingMovieList.append(contentsOf: movieList)
        sut.nowPlayingMovieList.append(contentsOf: movieList)
        
        StorageManager.setCacheTime(date: Date(timeIntervalSinceNow: 100))
    
        sut.checkCache()
        
        let promise = expectation(description: "movies loaded")
        let timeout: TimeInterval = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        
        XCTAssert(sut.popularMovieList.count == movieList.count * 2, "Movies should not be reloaded while cache is not obsolete")
        XCTAssert(sut.nowPlayingMovieList.count == movieList.count * 2, "Movies should not be reloaded while cache is not obsolete")
        XCTAssertFalse(movieListDelegate.dataWasRefreshed, "New movies should not be prompted to reload in table view")
        XCTAssertFalse(nowPlayingDelegate.dataWasRefreshed, "New movies should not be prompted to reload in collection view")
    }
    
    func testCacheManagement_appLaunchAfterCacheObsoletes() {
        sut.popularMovieList.append(contentsOf: movieList)
        sut.popularMovieList.append(contentsOf: movieList)
        sut.nowPlayingMovieList.append(contentsOf: movieList)
        sut.nowPlayingMovieList.append(contentsOf: movieList)
        
        StorageManager.setCacheTime(date: Date(timeIntervalSinceNow: -1))
        
        sut.checkCache()
        
        let promise = expectation(description: "movies loaded")
        let timeout: TimeInterval = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        
        XCTAssert(sut.popularMovieList.count == movieList.count, "Movies should be reloaded while cache is not obsolete")
        XCTAssert(sut.nowPlayingMovieList.count == movieList.count, "Movies should be reloaded while cache is not obsolete")
        XCTAssert(movieListDelegate.dataWasRefreshed, "New movies should be prompted to reload in table view")
        XCTAssert(nowPlayingDelegate.dataWasRefreshed, "New movies should be prompted to reload in collection view")
    }
}
