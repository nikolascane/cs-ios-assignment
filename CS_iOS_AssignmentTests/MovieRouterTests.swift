//
//  MovieListRouterTests.swift
//  CS_iOS_AssignmentTests
//
//  Created by Nik Cane on 30.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import XCTest

@testable import CS_iOS_Assignment

class MovieRouterTests: XCTestCase {

    var sut: RouterProtocol!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = MoviesRouter()
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        sut = nil
    }

    func testRoutesList() throws {
        let routes: [Route.Path] = [.movieDetails]
        XCTAssertEqual(routes, sut.routes, "MoviesRouter should contain appropriate list of routes")
    }
    
    func testConfigList() {
        let configs: [ConfigPath] = [.movieList, .nowPlaying]
        XCTAssertEqual(configs, sut.configs, "MoviesRouter should contain appropriate list of configs")
    }
    
    func testTypeOfPath() {
        let movieListType = MovieListViewController.self
        let nowPlayingPath = NowPlayingCollectionViewController.self
        let movieDetailsPath = MovieDetailsViewController.self
        
        let movieListTypeTest = sut.typeOf(path: .movieList)
        let nowPlayingPathTest = sut.typeOf(path: .nowPlaying)
        let movieDetailsPathTest = sut.typeOf(path: .movieDetails)
        
        XCTAssert(movieListType == movieListTypeTest, "Types should be equal for .movieList config")
        XCTAssert(nowPlayingPath == nowPlayingPathTest, "Types should be equal for .nowPlaying config")
        XCTAssert(movieDetailsPath == movieDetailsPathTest, "Types should be equal for .movieDetails config")
    }
    
    func testCurrentStoryboardName() {
        let storyboardName = "Main"
        XCTAssertEqual(sut.currentStoryboardName, storyboardName, "MoviesRouter should have concrete storyboard name")
    }
    
    func testCreateViewController() {
        let action = RouteAction(config: MovieViewModel(),
                                 route: Route(path: .movieDetails),
                                 configPath: nil,
                                 presentationMode: .modalTransparent)
        let viewController = sut.createViewController(action: action)
        XCTAssertNotNil(viewController, "")
        XCTAssert(type(of: viewController!) == MovieDetailsViewController.self, "")
    }
    
    func testConfigViewController() {
        let viewModel = MovieViewModel()
        let action = RouteAction(config: viewModel, route: nil, configPath: .nowPlaying, presentationMode: .none)
        let nowPlaying = NowPlayingViewModelDelegateMock()
        let success = sut.config(viewController: nowPlaying, action: action)
        XCTAssertTrue(success, "Config should pass successfully")
        XCTAssertNotNil(nowPlaying.viewModel, "viewModel property should be assigned")
        XCTAssertNotNil(viewModel.nowPlayingDelegate, "nowPlayingDelegate should be assigned")
    }
}
