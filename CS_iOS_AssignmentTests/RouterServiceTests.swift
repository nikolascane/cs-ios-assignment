//
//  MovieListRouterTests.swift
//  CS_iOS_AssignmentTests
//
//  Created by Nik Cane on 30.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import XCTest
@testable import CS_iOS_Assignment

class RouterServiceTests: XCTestCase {
    var sut: RouterService!

    override func setUpWithError() throws {
        try super.setUpWithError()
        
        sut = ServiceManager.shared.routerService
        sut.routers = [RouterMock()]
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        sut = nil
    }

    func testHandleConfigAction() throws {
        let action = RouteAction(config: MovieViewModel(), route: nil, configPath: .movieList, presentationMode: .none)
        let success = sut.handleConfig(action: action, for: MovieListViewController())
        XCTAssertTrue(success, "Config should be handled")
    }
    
    func testHandleCreateAction() {
        let promise = expectation(description: "View controller presented")
        let action = RouteAction(config: MovieViewModel(), route: Route(path: .movieDetails), configPath: nil, presentationMode: .modalTransparent)
        
        try! sut.handleCreate(action: action)
        let timeout: TimeInterval = 1
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        
        let topViewController = try! sut.topViewController()
        XCTAssert(topViewController is MovieDetailsViewController, "Route service shout put MovieDetailsViewController on the top of the view stack")
    }
}
