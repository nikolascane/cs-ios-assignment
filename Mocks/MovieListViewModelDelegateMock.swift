//
//  MovieListViewModelDelegateMock.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 29.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

class MovieListViewModelDelegateMock: MovieListViewModelProtocol {
    var viewModel: MovieListProtocol?
    var newCallsIndexPaths: [IndexPath]?
    var oldCellIndexPath: IndexPath?
    var activityStarted: Bool = false
    var activityStopped: Bool = false
    var dataWasRefreshed: Bool = false
    
    func config(viewModel: MovieListProtocol) {
        self.viewModel = viewModel
    }
    
    func reloadNewCellsFor(indexPaths: [IndexPath], refresh: Bool) {
        self.newCallsIndexPaths = indexPaths
        self.dataWasRefreshed = refresh
    }
    
    func reloadOldCellsFor(indexPath: IndexPath) {
        self.oldCellIndexPath = indexPath
    }
    
    func startActivity() {
        self.activityStarted = true
    }
    
    func stopActivity() {
        self.activityStopped = true
    }
}
