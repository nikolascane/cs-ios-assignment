//
//  NowPlayingViewModelDelegateMock.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 29.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class NowPlayingViewModelDelegateMock: UIViewController, NowPlayingMoviesViewModelProtocol {
    var viewModel: NowPlayingMoviesProtocol?
    var newCallsIndexPaths: [IndexPath]?
    var oldCellIndexPath: IndexPath?
    var dataWasRefreshed: Bool = false
    
    func config(viewModel: NowPlayingMoviesProtocol) {
        self.viewModel = viewModel
    }
    
    func reloadNewCellsFor(indexPaths: [IndexPath], refresh: Bool) {
        self.newCallsIndexPaths = indexPaths
        self.dataWasRefreshed = refresh
    }
    
    func reloadOldCellsFor(indexPath: IndexPath) {
        self.oldCellIndexPath = indexPath
    }
}
