//
//  RouterMock.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 29.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class RouterMock: RouterProtocol {
    let routes: [Route.Path] = [.movieDetails]
    let configs: [ConfigPath] = [.movieList]
    
    func typeOf(path: ConfigPath) -> UIViewController.Type {
        switch path {
        case .movieList:
            return MovieListViewController.self
        default:
            return UIViewController.self
        }
    }
    
    var currentStoryboardName: String = {
       "Main"
    }()
    
    func createViewController(action: RouteAction) -> UIViewController? {
        var route = action.route!
        switch route.path {
        case .movieDetails:
            route.screenName = "MovieDetailsViewController"
            route.storyboardName = self.currentStoryboardName
            let viewController = self.instantiateViewController(route: route) as! MovieDetailsViewController
            let viewModel = action.config as! MovieDetailsProtocol & MovieViewModelConfigProtocol
            viewController.config(viewModel: viewModel)
            viewModel.movieDetailsDelegate = viewController
            return viewController
        default:
            return nil
        }
    }
    
    func config(viewController: UIViewController, action: RouteAction) -> Bool {
        switch action.configPath {
        case .movieList:
            let viewController = viewController as! MovieListViewController
            let viewModel = action.config as! MovieListProtocol & MovieViewModelConfigProtocol
            viewController.config(viewModel: viewModel)
            viewModel.movieListDelegate = viewController
            return true
        default:
            return false
        }
    }
}
