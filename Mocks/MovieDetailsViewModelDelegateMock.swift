//
//  MovieDetailsViewModelDelegateMock.swift
//  CS_iOS_Assignment
//
//  Created by Nik Cane on 29.04.2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

class MovieDetailsViewModelDelegateMock: MovieDetailsViewModelProtocol {
    var posterData: Data?
    
    func reloadImageWithPoster(data: Data) {
        self.posterData = data
    }
}
