//
//  CS_iOS_AssignmentUITests.swift
//  CS_iOS_AssignmentUITests
//
//  Created by Vipul Shah on 23/12/2019.
//  Copyright © 2019 Backbase. All rights reserved.
//

import XCTest
@testable import CS_iOS_Assignment

class CS_iOS_AssignmentUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        app = XCUIApplication()
        app.launch()
        continueAfterFailure = false
    }

    override func tearDown() {
        super.tearDown()
        app = nil
    }

    func testMainScreen() {
        let movieListTableviewTable = app.tables["Movie List TableView"]
        let nowPlayingHeader = movieListTableviewTable.otherElements["Now playing header"]
        let mostPopularHeader = movieListTableviewTable.otherElements["Most popular header"]
        
        XCTAssert(nowPlayingHeader.exists, "Now playing header displaying error")
        XCTAssert(mostPopularHeader.exists, "Most popular header displaying error")
    }
    
    func testMovieListTableView() {
        let movieListTableviewTable = app.tables["Movie List TableView"]
        let movieListTableviewCell = movieListTableviewTable.cells["Movie TableView List Cell 4"]
        movieListTableviewCell.swipeDown()
        movieListTableviewCell.swipeUp()
        
        XCTAssert(movieListTableviewTable.exists, "Movie List TableView displaying error")
        XCTAssert(movieListTableviewCell.exists, "Movie List TableView displaying error")
    }
    
    func testMovieListCollectionView() {
        let nowPlayingMoviesCollectionviewCollectionView = app.tables["Movie List TableView"].collectionViews["Now Playing Movies CollectionView"]
        let cell = nowPlayingMoviesCollectionviewCollectionView.cells["Now Playing Movies CollectionView Cell 1"]
        cell.swipeRight()
        cell.swipeLeft()

        XCTAssert(nowPlayingMoviesCollectionviewCollectionView.exists, "Collection view should be presented")
        XCTAssertFalse(cell.exists, "Collection view cell should be presented")
    }
    
    func testDetailsScreen() {
        let movieListTableviewTable = app.tables["Movie List TableView"]
        movieListTableviewTable.collectionViews["Now Playing Movies CollectionView"].cells["Now Playing Movies CollectionView Cell 1"].otherElements.containing(.image, identifier:"Now Playing Movies CollectionView Cell Poster").element.tap()
    
        let promise = expectation(description: "wait for details screen to present")
        let timeout: TimeInterval = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
            promise.fulfill()
        }
        wait(for: [promise], timeout: timeout)
        
        let posterImage = app.images["Detail Screen PosterImage"]
        let borderView = app.otherElements["Detail Screen BorderView"]
        let movieNameLabel = app/*@START_MENU_TOKEN@*/.staticTexts["Detail Screen MovieName"]/*[[".staticTexts[\"Fantasy Island\"]",".staticTexts[\"Detail Screen MovieName\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let runtimeLabel = app/*@START_MENU_TOKEN@*/.staticTexts["Detail Screen ReleaseDataRuntime"]/*[[".staticTexts[\"February 12, 2020 - 1h 49m\"]",".staticTexts[\"Detail Screen ReleaseDataRuntime\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let overviewLabel = app/*@START_MENU_TOKEN@*/.staticTexts["Detail Screen Overview"]/*[[".staticTexts[\"Overview\"]",".staticTexts[\"Detail Screen Overview\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let overviewDescription = app/*@START_MENU_TOKEN@*/.staticTexts["Detail Screen OverviewText"]/*[[".staticTexts[\"A group of contest winners arrive at an island hotel to live out their dreams, only to find themselves trapped in nightmare scenarios.\"]",".staticTexts[\"Detail Screen OverviewText\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let tagView = app.otherElements["Detail Screen TagView"]
        let closeButton = app/*@START_MENU_TOKEN@*/.buttons["Detail Screen CloseButton"]/*[[".buttons[\"multiply\"]",".buttons[\"Detail Screen CloseButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        
        XCTAssert(borderView.exists, "Detail Screen BorderView display error")
        XCTAssert(movieNameLabel.exists, "Detail Screen MovieName display error")
        XCTAssert(runtimeLabel.exists, "Detail Screen ReleaseDataRuntime display error")
        XCTAssert(overviewLabel.exists, "Detail Screen Overview display error")
        XCTAssert(overviewDescription.exists, "Detail Screen OverviewText display error")
        XCTAssert(tagView.exists, "Detail Screen TagView display error")
        XCTAssert(closeButton.exists, "Detail Screen CloseButton display error")
        XCTAssert(posterImage.exists, "Detail Screen PosterImage display error")
        
        closeButton.tap()
    }
    
    
    func testMovieTableViewCell() {
        let cell = app.tables["Movie List TableView"].cells["Movie TableView List Cell 1"]
        
        let ratingView = cell.otherElements["Movie TableView List Cell Rating"]
        let borderView = cell.otherElements["Movie TableView List Cell BorderView"]
        let posterImage = cell.images["Movie TableView List Cell Poster"]
        let titleLabel = cell.staticTexts["Movie TableView List Cell Title"]
        let releaseDateLabel = cell.staticTexts["Movie TableView List Cell ReleaseDate"]
        let runtimeLabel = cell.staticTexts["Movie TableView List Cell Runtime"]
        let dividerView = cell.otherElements["Movie TableView List Cell DividerView"]
    
        XCTAssert(ratingView.exists, "Movie TableView List Cell Rating display error")
        XCTAssert(borderView.exists, "Movie TableView List Cell BorderView display error")
        XCTAssert(posterImage.exists, "Movie TableView List Cell Poster display error")
        XCTAssert(titleLabel.exists, "Movie TableView List Cell Title display error")
        XCTAssert(releaseDateLabel.exists, "Movie TableView List Cell ReleaseDate display error")
        XCTAssert(runtimeLabel.exists, "Movie TableView List Cell Runtime display error")
        XCTAssert(dividerView.exists, "Movie TableView List Cell DividerView display error")
    }
    
    func testMovieCollectionViewCell() {
        let cell = app.tables["Movie List TableView"].collectionViews["Now Playing Movies CollectionView"].cells["Now Playing Movies CollectionView Cell 1"]
        let posterImage = cell.images["Now Playing Movies CollectionView Cell Poster"]
        
        XCTAssert(cell.exists, "Now Playing Movies CollectionView displaying error")
        XCTAssert(posterImage.exists, "Now Playing Movies CollectionView Cell Poster displaying error")
    }
}
